# Установка версии
`make set_version`

# Коммит и пуш
`make fix`

# Запуск dev
`make run`

# Собираем базовый образ
`make docker_build_baseimage`

# собрать образ локально
`make docker_build`

# собрать и запушить образ backend
`make docker_push`

# запустить докер с backend
`make docker_run`

# Деплой
`make deploy`
#Генерируем GRPC для Go
grpc_generate_golang:
	rm -rf ./services/downloader/proto/*.Go
	protoc --go_out=plugins=grpc:. ./services/downloader/proto/*.proto


	rm -rf ./services/starter/proto/*.Go
	protoc --go_out=plugins=grpc:. ./services/starter/proto/*.proto

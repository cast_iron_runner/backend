package db

type StatusDB interface {
	SendInfo(url, name, status string) error
}

func NewFakeDatabase() StatusDB {
	return &FakeDatabase{}
}

type FakeDatabase struct {
}

func (fdb *FakeDatabase) SendInfo(url, name, status string) error {
	return nil
}

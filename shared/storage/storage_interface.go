package storage

type Storage interface{
	SendCSV(name string, csv []byte) (error)
}

type FakeStorage struct{

}

func NewFakeStorage() Storage{
	return &FakeStorage{}
}

func (fsg *FakeStorage) SendCSV(name string, csv []byte) error{
	return nil
}
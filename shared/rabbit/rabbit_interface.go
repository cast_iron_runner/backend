package rabbit

type Rabbit interface {
	SendTask(url, name string) error
	ReceiveTask(url, name string)
}

func NewFakeRabbit() Rabbit {
	return &FakeRabbit{}
}

type FakeRabbit struct {
}

func (frb *FakeRabbit) SendTask(url, name string) error {
	return nil
}

func (frb *FakeRabbit) ReceiveTask(url, name string) {
	
}
# НЕ ЗАБУДЬ ОБНОВИТЬ ВЕРСИЮ baseimage
FROM registry.gitlab.com/cast_iron_runner/backend/baseimage:0.1.3 as build
WORKDIR /go/src/app
COPY . .
ENV GOPATH /go/src/app
RUN go build -o main

FROM alpine:3.9
COPY --from=build /go/src/app/main /usr/local/bin/main
ENTRYPOINT ["/usr/local/bin/main"]


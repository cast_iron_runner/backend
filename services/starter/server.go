package starter

import (
	context "context"
	"fmt"
	"log"
	"net"

	"../../shared/db"
	pb "./proto"

	"../../shared/rabbit"
	grpc "google.golang.org/grpc"
)

type Server struct {
	db db.StatusDB
	rb rabbit.Rabbit
}

func NewServer(db db.StatusDB, rb rabbit.Rabbit) (*Server, error) {
	srv := &Server{
		db: db,
		rb: rb,
	}
	return srv, nil
}

func (s *Server) Run(port int) error {
	srv := grpc.NewServer()
	pb.RegisterStarterServer(srv, s)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	return srv.Serve(lis)
}

func (s Server) StartJob(ctx context.Context, in *pb.JobRequest) (*pb.JobResponse, error) {
	//goroutines

	for _, csvInfo := range in.CSVs {
		s.rb.SendTask(csvInfo.URL, csvInfo.Name)
		//in-progress enum
		s.db.SendInfo(csvInfo.URL, csvInfo.Name, "in-progress")
	}

	return nil, nil
}

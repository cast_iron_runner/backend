package downloader

import (
	"io/ioutil"
	//"encoding/json"
	//"os"
	"bufio"
	context "context"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"

	pb "../downloader/proto"
	"google.golang.org/grpc"

	"../../shared/storage"
)

type Server struct {
	storage storage.Storage
}

func NewServer(storage storage.Storage) (*Server, error) {
	srv := &Server{
		storage: storage,
	}
	return srv, nil
}

func (s *Server) Run(port int) error {
	srv := grpc.NewServer()
	pb.RegisterDownloaderServer(srv, s)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	return srv.Serve(lis)
}

type ParsedCSVInfo struct {
	ID     string
	URL    string
	Cols   []string
	Status string
}

var urls []string = []string{
	"https://data.gov.ru/sites/default/files/3eba4626_perechen_avtozapravochnyh_stanciy.csv",
	"https://data.gov.ru/sites/default/files/6770bd42_perechen_aptek_raspolozhennyh_na_territorii_municipaliteta.csv",
	"https://data.gov.ru/sites/default/files/ccfe30db_perechen_bibliotek.csv",
	"https://data.gov.ru/sites/default/files/7c919ced_perechen_doshkolnyh_obrazovatelnyh_organizaciy.csv",
	"https://data.gov.ru/sites/default/files/2cec3d21_perechen_medicinskih_uchrezhdeniy_1.csv",
	"https://data.gov.ru/sites/default/files/5cbea307_perechen_muzeev.csv",
}

func (server *Server) parseColumns(url string) ([]string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	scanner := bufio.NewScanner(resp.Body)
	scanner.Scan() //return bool
	firstLine := scanner.Text()

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	//fmt.Printf("Line: %s\n", firstLine)

	columns := strings.Split(firstLine, ",")
	for index, row := range columns {
		colLength := len(row)

		row = row[1 : colLength-1]
		columns[index] = row
	}

	return columns, nil
}

func (server *Server) downloadFile(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}

func (server Server) StartJob(ctx context.Context, in *pb.DownloadRequest) (*pb.DownloadResponse, error) {
	//TODO goroutines
	//for _, csvInfo := range in.CSVs {

	//cols, err := server.parseColumns(csvInfo.URL)
	//}

	return nil, nil
}

package httpdownloader

import (
	"bufio"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	//"strings"
	//"net/url"
)

type Server struct {
}

func NewServer() (*Server, error) {
	srv := &Server{}
	return srv, nil
}

func (s *Server) Run(endp string, port int) error {
	http.HandleFunc(endp, s.handler)

	return http.ListenAndServe(":1337", nil)
}

func (s *Server) handler(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}

	dec := json.NewDecoder(r.Body)
	var req ReqData
	dec.Decode(&req)
	urls := strings.Split(req.Data, "\n")

	resp := make([]ParsedCSVInfo, 0)

	for _, url := range urls {
		cols, err := s.parseColumns(url)

		errStr := ""
		if err != nil {
			errStr = err.Error()
		}
		parsed := ParsedCSVInfo{
			URL:   url,
			Cols:  cols,
			Error: errStr,
		}
		resp = append(resp, parsed)
	}

	bytes, _ := json.Marshal(resp)
	w.Write(bytes)
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

type ReqData struct {
	Data string `json:"data"`
}

type ParsedCSVInfo struct {
	URL   string   `json:"url"`
	Cols  []string `json:"cols"`
	Error string   `json:"error"`
}

var constURLs []string = []string{
	"https://data.gov.ru/sites/default/files/3eba4626_perechen_avtozapravochnyh_stanciy.csv",
	"https://data.gov.ru/sites/default/files/6770bd42_perechen_aptek_raspolozhennyh_na_territorii_municipaliteta.csv",
	"https://data.gov.ru/sites/default/files/ccfe30db_perechen_bibliotek.csv",
	"https://data.gov.ru/sites/default/files/7c919ced_perechen_doshkolnyh_obrazovatelnyh_organizaciy.csv",
	"https://data.gov.ru/sites/default/files/2cec3d21_perechen_medicinskih_uchrezhdeniy_1.csv",
	"https://data.gov.ru/sites/default/files/5cbea307_perechen_muzeev.csv",
}

func (s *Server) parseColumns(url string) ([]string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	scanner := bufio.NewScanner(resp.Body)
	scanner.Scan() //return bool
	firstLine := scanner.Text()

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	//fmt.Printf("Line: %s\n", firstLine)

	columns := strings.Split(firstLine, ",")
	for index, row := range columns {
		colLength := len(row)

		row = row[1 : colLength-1]
		columns[index] = row
	}

	return columns, nil
}

func (server *Server) downloadFile(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}

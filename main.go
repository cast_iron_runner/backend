package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"./services/downloader"
	"./services/httpdownloader"
	"./services/starter"
	"./shared/db"
	"./shared/rabbit"
	"./shared/storage"
)

func usage() {
	fmt.Fprintf(os.Stderr, "USAGE\n")
	fmt.Fprintf(os.Stderr, "  serve <mode> [flags]\n")
	fmt.Fprintf(os.Stderr, "\n")
	fmt.Fprintf(os.Stderr, "Services\n")
	fmt.Fprintf(os.Stderr, "  all          Boots all services\n")
	fmt.Fprintf(os.Stderr, "  downloader   Работяга качающий CSVшки\n")
	fmt.Fprintf(os.Stderr, "  starter      Заставляет работяг работать\n")
	fmt.Fprintf(os.Stderr, "\n")
}

func main() {
	srv, _ := httpdownloader.NewServer()
	panic(srv.Run("/send", 1337))
	return
	//
	//port := flag.Int("port", 1337, "The server port")

	flag.Parse()
	if len(os.Args) < 1 {
		usage()
		os.Exit(1)
	}

	switch strings.ToLower(os.Args[1]) {
	case "all":
		starterService, _ := starter.NewServer(db.NewFakeDatabase(), rabbit.NewFakeRabbit())
		downloaderService, _ := downloader.NewServer(storage.NewFakeStorage())

		starterService.Run(8000)
		downloaderService.Run(8001)
	case "starter":
		starterService, _ := starter.NewServer(db.NewFakeDatabase(), rabbit.NewFakeRabbit())
		starterService.Run(8000)
	case "downloader":
		downloaderService, _ := downloader.NewServer(storage.NewFakeStorage())
		downloaderService.Run(8001)
	default:
		usage()
		os.Exit(1)
	}
}

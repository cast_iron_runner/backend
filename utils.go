package main

import (
	"os"
)

func GetENV(name string, default_value string) string {
	val := os.Getenv(name)
	if val == "" {
		val = default_value
	}

	return val
}
